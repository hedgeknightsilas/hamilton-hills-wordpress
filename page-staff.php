<?php /* Template Name: Staff */ ?>

<?php get_header(); ?>

  <main class="site-main" role="main">
    <div class="page-content-wrapper">
      <div class="padding-wrapper medium-wrapper">

        <h2>Pastors</h2>

        <?php
          $args = array(
            'post_type' => 'staff_members',
            'orderby' => 'date',
            'order' => 'ASC',
            'meta_key' => 'staff_type',
            'meta_value' => 'pastor',
          );
          $query = new WP_Query( $args );
        ?>

        <?php if( $query->have_posts() ): ?>

          <?php while( $query->have_posts() ): $query->the_post(); ?>

            <div class="staff">

              <?php 
                $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'hh-medium'); 
              ?>

              <div class="staff__image" style="background-image: url(<?php echo $url; ?>);">
                
              </div>

              <div class="staff__body">

                <h3><?php the_title(); ?></h3>

                <p class="staff__title"><?php the_field( 'title' ); ?></p>

                <p class="staff__short-bio"><?php the_field( 'short_bio'); ?></p>

                <div class="button-wrapper">
                  <a class="button button--tertiary" href="<?php the_permalink(); ?>">Read Full Bio</a>
                </div>

              </div>

            </div>

          <?php endwhile; ?>

        <?php endif; wp_reset_query(); ?>

        <?php
          $args = array(
            'post_type' => 'staff_members',
            'orderby' => 'date',
            'order' => 'ASC',
            'meta_key' => 'staff_type',
            'meta_value' => 'staff',
          );
          $query = new WP_Query( $args );
        ?>

        <?php if( $query->have_posts() ): ?>

          <h2>Campus Staff</h2>

          <div class="staff-grid">

            <?php while( $query->have_posts() ): $query->the_post(); ?>

              <div class="staff staff--compact">

                <?php 
                  $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'hh-medium'); 
                ?>

                <div class="staff__image" style="background-image: url(<?php echo $url; ?>);">
                  
                </div>

                <div class="staff__body">

                  <h3><?php the_title(); ?></h3>

                  <p class="staff__title"><?php the_field( 'title' ); ?></p>

                  <div class="text-link-wrapper">

                    <a class="text-link" href="<?php the_permalink(); ?>">
                      <span>Read Bio</span>
                      <svg xmlns="http://www.w3.org/2000/svg" width="9.681" height="15" viewBox="0 0 9.681 15">
                        <path id="angle-right-solid" d="M34.1,104.247l-6.373,6.373a1.12,1.12,0,0,1-1.589,0l-1.059-1.059a1.12,1.12,0,0,1,0-1.589l4.517-4.517L25.08,98.937a1.12,1.12,0,0,1,0-1.589l1.054-1.068a1.12,1.12,0,0,1,1.589,0l6.373,6.373A1.121,1.121,0,0,1,34.1,104.247Z" transform="translate(-24.75 -95.95)" fill="#2cb3db"/>
                      </svg>
                    </a>
                  </div>

                </div>

              </div>

            <?php endwhile; ?>

          </div>

        <?php endif; ?>
    
      </div>
    </div>
  </main>

<?php get_footer(); ?>
