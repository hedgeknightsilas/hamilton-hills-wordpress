  </div><!-- #content -->

  <footer class="site-footer">

    <div class="site-footer__logo">

      <?php if( function_exists( 'get_field' ) ): ?>

        <?php

          $image = get_field( 'footer_logo', 'options' );
          $size = 'logo';
          $src = $image['url'];
          $alt = $image['alt'];
          $title = $image['title'];
          $thumb = $image['sizes'][ $size ];
          $caption = $image['caption'];

        if( $image ): ?>

          <a href="<?php echo esc_url( home_url() ); ?>">

            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

          </a>

        <?php endif; ?>

      <?php endif; ?>

    </div>

    <?php if( have_rows( 'social_media', 'options' ) ): ?>

      <div class="site-footer__social">

        <?php while( have_rows( 'social_media', 'options' ) ): the_row(); ?>

          <a class="site-footer__social-icon" href="<?php the_sub_field( 'social_link', 'options' ); ?>">
            <?php the_sub_field( 'social_icon', 'options' ); ?>
          </a>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>

    <div class="site-footer__address">
        
      <p><?php the_field( 'street_address', 'options' ); ?> | <?php the_field( 'locality', 'options' ); ?></p>

    </div>

    <a href="#page-top" class="scroll-button">
      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrow-up.svg" />
    </a>

  </footer>

<?php wp_footer(); ?>

</body>
</html>
