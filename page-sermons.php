<?php /* Template Name: Sermons */ ?>

<?php get_header(); ?>

  <main class="site-main" role="main">

    <?php if( function_exists( 'get_field' ) ): ?>

      <?php if( have_rows( 'content_blocks' ) ): ?>

        <?php while( have_rows( 'content_blocks' ) ): the_row(); ?>

          <?php if( get_row_layout() == 'icon_left' ): ?>

            <?php get_template_part('template-parts/content-block', 'icon-left'); ?>

          <?php elseif( get_row_layout() == 'image_background_with_buttons' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'image-background-with-buttons' ); ?>

          <?php elseif( get_row_layout() == 'image_left' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'image-left' ); ?>

          <?php elseif( get_row_layout() == 'basic_text_block' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'basic-text' ); ?>

          <?php elseif( get_row_layout() == 'centered_text_block' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'centered-text-block'); ?>

          <?php endif; ?>

        <?php endwhile; ?>

      <?php endif; ?>

    <?php endif; ?>

    <?php
      $args = array(
        'post_type' => 'post',
        'orderby' => 'date',
        'category_name' => 'sermons'
      );
      $query = new WP_Query( $args );
    ?>

    <?php if( $query->have_posts() ) : ?>

      <div class="content-block content-block__card-list">

        <div class="card-list">

          <?php while( $query->have_posts() ) : $query->the_post(); ?>
            <?php 
              $post = get_post( $post ); 
               $url = wp_get_attachment_url( get_post_thumbnail_id($post), 'medium'); 
            ?>

            <div class="card">

              <div class="card__image">
                <a href="<?php the_permalink(); ?>">

                  <img src="<?php echo $url; ?>" alt="">

                </a>
              </div>

              <div class="card__text">

                <?php the_title(); ?>

              </div>

            </div>

          <?php endwhile; ?>

      </div>

      <div class="page-links">

        <?php if( function_exists( 'wp_pagenavi') ): ?>

          <div class="navigation">

            <?php wp_pagenavi(); ?>

          </div>

        <?php else: ?>

          <?php the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'hamiltonhills' ),
            'next_text'          => __( 'Next page', 'hamiltonhills' ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'hamiltonhills' ) . ' </span>',
          ) ); ?>

        <?php endif; ?>

      </div>

    <?php endif; ?>

    
  </main>

<?php get_footer(); ?>
