<div class="content-block content-block__page-list <?php the_sub_field( 'background_color' ); ?> <?php the_sub_field( 'text_color' ); ?>">

  <div class="content-block__content">

  	<?php if( get_sub_field( 'section_heading' ) ): ?>

			<h2 class="section-heading"><?php the_sub_field( 'section_heading' ); ?></h2>

		<?php endif; ?>
		
		<?php if( get_sub_field( 'content' ) ): ?>

			<?php the_sub_field( 'content' ); ?>

		<?php endif; ?>

		<?php if( have_rows( 'page_list' ) ): ?>

	  	<div class="page-list">

	  		<?php while( have_rows( 'page_list' ) ): the_row(); ?>

	  			<div class="page-list__item">

		  			<div class="page-list__heading">

		  				<h3><?php the_sub_field( 'heading' ); ?></h3>

		  			</div>

	  				<div class="page-list__text">

	  					<?php the_sub_field( 'text' ); ?>

	  				</div>

	  				<div class="page-list__button">

	  					<a class="button <?php the_sub_field( 'button_type' ); ?>" href="<?php the_sub_field( 'page_link' ); ?>">
								<?php the_sub_field( 'button_text' ); ?>
							</a>

	  				</div>

	  			</div>

	  		<?php endwhile; ?>
	  		
	  	</div>

	  <?php endif; ?>

  </div>

</div>