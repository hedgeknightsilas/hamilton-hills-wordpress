<div class="content-block content-block-quote-section <?php the_sub_field( 'background_color' ); ?>">
	<div class="content-block__wrapper">

	  <div class="content-block__content">
			
			<?php if( get_sub_field( 'content' ) ): ?>

				<?php the_sub_field( 'content' ); ?>

			<?php endif; ?>

			<?php if( get_sub_field( 'quote_reference' ) ): ?>

				<p class="quote-reference">— <?php the_sub_field( 'quote_reference' ); ?></p>

			<?php endif; ?>

	  </div>

	</div>

</div>