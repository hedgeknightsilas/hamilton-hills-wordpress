<div class="content-block content-block__icon-list <?php the_sub_field( 'background_color' ); ?> <?php the_sub_field( 'text_color' ); ?>">

  <div class="content-block__content">

  	<?php if( get_sub_field( 'section_heading' ) ): ?>

			<h2 class="section-heading"><?php the_sub_field( 'section_heading' ); ?></h2>

		<?php endif; ?>
		
		<?php if( get_sub_field( 'content' ) ): ?>

			<?php the_sub_field( 'content' ); ?>

		<?php endif; ?>

		<?php if( have_rows( 'icon_list' ) ): ?>

	  	<div class="icon-list">

	  		<?php while( have_rows( 'icon_list' ) ): the_row(); ?>

	  			<div class="icon-list__item">

		  			<div class="icon-list__icon">

		  				<?php the_sub_field( 'icon' ); ?>

		  			</div>

	  				<div class="icon-list__text">

	  					<?php the_sub_field( 'text' ); ?>

	  				</div>

	  			</div>

	  		<?php endwhile; ?>
	  		
	  	</div>

	  <?php endif; ?>

  </div>

</div>