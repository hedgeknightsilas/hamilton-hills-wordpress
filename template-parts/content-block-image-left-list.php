<div class="content-block content-block-image-left">
	<div class="content-block__wrapper">
	
		<?php

	    $image = get_sub_field( 'image' );
	    $size = 'hh-medium';
	    $src = $image['url'];
	    $alt = $image['alt'];
	    $title = $image['title'];
	    $thumb = $image['sizes'][ $size ];
	    $caption = $image['caption'];

	  if( $image ): ?>

	  	<div class="content-block__image">

	    	<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

	    </div>

	  <?php endif; ?>

	  <div class="content-block__content">
			
			<?php if( get_sub_field( 'section_heading' ) ): ?>

				<h2><?php the_sub_field( 'section_heading' ); ?></h2>

			<?php endif; ?>

			<?php if( get_sub_field( 'text' ) ): ?>

				<?php the_sub_field( 'text' ); ?>

			<?php endif; ?>

			<?php if( get_sub_field( 'page_link' ) ): ?>

				<a class="button button--tertiary" href="<?php the_sub_field( 'page_link' ); ?>">
					<?php the_sub_field( 'button_text' ); ?>
				</a>

			<?php endif; ?>

			<?php if( have_rows( 'buttons' ) ): ?>

		  	<div class="content-block__button-wrapper">

		  		<?php while( have_rows( 'buttons' ) ): the_row(); ?>

		  			<?php if( get_sub_field( 'external_link?' ) ): ?>

							<a class="button <?php the_sub_field( 'button_type' ); ?>" href="<?php the_sub_field( 'external_link' ); ?>">
								<?php the_sub_field( 'button_text' ); ?>
							</a>

						<?php else: ?>

							<a class="button <?php the_sub_field( 'button_type' ); ?>" href="<?php the_sub_field( 'page_link' ); ?>">
								<?php the_sub_field( 'button_text' ); ?>
							</a>

						<?php endif; ?>

		  		<?php endwhile; ?>
		  		
		  	</div>

		  <?php endif; ?>

	  </div>

	</div>

</div>