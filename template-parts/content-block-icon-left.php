<div class="content-block content-block__icon-left">

  <?php if( get_sub_field( 'section_heading' ) ): ?>

    <h2><?php the_sub_field( 'section_heading' ); ?></h2>

  <?php endif; ?>

  <?php if( have_rows( 'content_block_item' ) ): ?>

    <?php while( have_rows( 'content_block_item' ) ): the_row(); ?>

      <div class="content-block__item">

        <div class="content-block__body">
          <div class="content-block__header">
            <div class="content-block__icon">

              <?php

                $image = get_sub_field( 'icon' );
                $size = 'icon';
                $src = $image['url'];
                $alt = $image['alt'];
                $thumb = $image['sizes'][ $size ];

              if( $image ): ?>

                <img style="max-width: 200px" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

              <?php endif; ?>
              
            </div>
            <h3><?php the_sub_field( 'heading' ); ?></h3>
          </div>

          <div class="content-block__text">
            <?php the_sub_field( 'text' ); ?>
          </div>

          <div class="content-block__button">
            <a class="button button--tertiary" href="<?php the_sub_field( 'page_link' ); ?>"><?php the_sub_field( 'button_text' ); ?></a>
          </div>
        </div>

      </div>

    <?php endwhile; ?>

  <?php endif; ?>

</div>