<div class="content-block content-block-basic-text <?php the_sub_field( 'background_color' ); ?> <?php the_sub_field( 'text_color' ); ?>">

  <div class="content-block__content">

  	<?php if( get_sub_field( 'section_heading' ) ): ?>

			<h2><?php the_sub_field( 'section_heading' ); ?></h2>

		<?php endif; ?>
		
		<?php if( get_sub_field( 'content' ) ): ?>

			<?php the_sub_field( 'content' ); ?>

		<?php endif; ?>

		<?php if( have_rows( 'buttons' ) ): ?>

	  	<div class="content-block__button-wrapper">

	  		<?php while( have_rows( 'buttons' ) ): the_row(); ?>

	  			<?php if( get_sub_field( 'external_link?' ) ): ?>

						<a class="button <?php the_sub_field( 'button_type' ); ?>" href="<?php the_sub_field( 'external_link' ); ?>">
							<?php the_sub_field( 'button_text' ); ?>
						</a>

					<?php else: ?>

						<a class="button <?php the_sub_field( 'button_type' ); ?>" href="<?php the_sub_field( 'page_link' ); ?>">
							<?php the_sub_field( 'button_text' ); ?>
						</a>

					<?php endif; ?>

	  		<?php endwhile; ?>
	  		
	  	</div>

	  <?php endif; ?>

  </div>

</div>