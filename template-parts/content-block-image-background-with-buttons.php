<?php
  $image = wp_get_attachment_image_src( get_sub_field( 'background_image' ), 'banner' );
?>

<div class="content-block content-block__image-background-with-buttons" style="background-image: url(<?php echo $image[0]; ?>);">

	<div class="content-block__content-wrapper">

		<?php if( get_sub_field( 'section_heading' ) ): ?>

	    <h2><?php the_sub_field( 'section_heading' ); ?></h2>

	  <?php endif; ?>

	  <?php if( get_sub_field( 'text' ) ): ?>

	  	<p><?php the_sub_field( 'text' ); ?></p>

	  <?php endif; ?>

	  <?php if( have_rows( 'buttons' ) ): ?>

	  	<div class="content-block__button-wrapper">

	  		<?php while( have_rows( 'buttons' ) ): the_row(); ?>

	  			<?php if( get_sub_field( 'external_link?' ) ): ?>

						<a class="button button--ghost" href="<?php the_sub_field( 'external_link' ); ?>">
							<?php the_sub_field( 'button_text' ); ?>
						</a>

					<?php else: ?>

						<a class="button button--ghost" href="<?php the_sub_field( 'page_link' ); ?>">
							<?php the_sub_field( 'button_text' ); ?>
						</a>

					<?php endif; ?>

	  		<?php endwhile; ?>
	  		
	  	</div>

	  <?php endif; ?>

	</div>

</div>