<div class="content-block content-block__card-list">

	<div class="content-block__content-wrapper">

		<?php if( get_sub_field( 'section_heading' ) ): ?>

	    <h2><?php the_sub_field( 'section_heading' ); ?></h2>

	  <?php endif; ?>

	  <?php if( get_sub_field( 'text' ) ): ?>

	  	<p><?php the_sub_field( 'text' ); ?></p>

	  <?php endif; ?>

	  <?php if( have_rows( 'cards' ) ): ?>

	  	<div class="cards">

	  		<?php while( have_rows( 'cards' ) ): the_row(); ?>

	  			<div class="card">

	  				<?php

					    $image = get_sub_field( 'image' );
					    $size = 'hh-medium';
					    $src = $image['url'];
					    $alt = $image['alt'];
					    $title = $image['title'];
					    $thumb = $image['sizes'][ $size ];
					    $caption = $image['caption'];

					  if( $image ): ?>

					  	<div class="card__image">

					    	<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

					    </div>

					  <?php endif; ?>

					  <div class="card__text">

					  	<?php the_sub_field( 'card_name' ); ?>

					  </div>

	  			</div>

	  		<?php endwhile; ?>
	  		
	  	</div>

	  <?php endif; ?>

	</div>

</div>