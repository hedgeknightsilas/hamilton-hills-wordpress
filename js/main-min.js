jQuery(document).ready(function(o){function t(){return window.pageYOffset||document.documentElement.scrollTop}
// Content Block Animations
o("body").fitVids(),
//Smooth Scroll
// Select all links with hashes
o('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(e){
// On-page links
if(location.pathname.replace(/^\//,"")===this.pathname.replace(/^\//,"")&&location.hostname===this.hostname){
// Figure out element to scroll to
var t=o(this.hash);
// Does a scroll target exist?
(t=t.length?t:o("[name="+this.hash.slice(1)+"]")).length&&(
// Only prevent default if animation is actually gonna happen
e.preventDefault(),o("html, body").animate({scrollTop:t.offset().top-100},1e3,function(){
// Callback after animation
// Must change focus!
var e=o(t);if(e.focus(),e.is(":focus"))
// Checking if the target was focused
return!1;e.attr("tabindex","-1"),// Adding tabindex for elements not focusable
e.focus()}))}}),960<o(window).width()&&
// Scroll triggered Animations
o(".content-block__icon").addClass("hidden").viewportChecker({classToRemove:"hidden",classToAdd:"rollIn",offset:300});
//Handle sticky header
var n=90;o(window).scroll(function(){var e=t();n<=e?o(".site-header .flex-wrapper").addClass("sticky"):o(".site-header .flex-wrapper").removeClass("sticky")});for(var e=document.querySelectorAll(".content-block__icon-left .content-block__icon"),a=0;a<e.length;a++){var c=400*a;e[a].style.animationDelay=String(c)+"ms"}});