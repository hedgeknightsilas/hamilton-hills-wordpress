<?php get_header(); ?>

  <main class="site-main subpage" role="main">
    <div class="page-content-wrapper">
      <div class="padding-wrapper">

          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="page-content">

              <aside class="staff-info-wrapper">

                <?php 
                  $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'hh-medium'); 
                ?>

                <div class="staff-image" style="background-image: url(<?php echo $url; ?>);">
                  
                </div>

                <div class="staff-info">

                  <p class="staff-name"><?php the_title(); ?></p>
                  <p class="staff-title"><?php the_field( 'title' ); ?></p>

                </div>

              </aside>

              <div class="staff-bio">

                <div class="text-wrapper">

                  <?php the_content(); ?>

                </div>

              </div>

            </div>

          <?php endwhile; endif; ?>

          <?php if( get_post_type() == 'staff_members' ): ?>

            <div class="button-wrapper">

              <a href="/our-staff" class="button button--tertiary">< Back to all staff</a>

            </div>

          <?php endif; ?>

        </div>

      </div>
    </div>
  </main>

<?php get_footer(); ?>
