<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php bloginfo('description'); ?>">
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->

   <?php wp_head(); ?>

	<?php if( function_exists( 'get_field' ) ): ?>

		<?php if( get_field( 'google_analytics', 'options' ) ): ?>

			<?php the_field( 'google_analytics', 'options' ); ?>

		<?php endif; ?>

	<?php endif; ?>
</head>

<body <?php body_class(); ?> id="page-top">
	<a class="skip-main screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hedge_knight' ); ?></a>

  <header class="site-header">

			<div class="flex-wrapper">
				<div class="site-header__logo">

					<div class="full-logo">
					
						<?php if( function_exists( 'get_field' ) ): ?>

							<?php

			          $image = get_field( 'header_logo', 'options' );
			          $size = 'logo';
			          $src = $image['url'];
			          $alt = $image['alt'];
			          $title = $image['title'];
			          $thumb = $image['sizes'][ $size ];
			          $caption = $image['caption'];

			        if( $image ): ?>

			          <a href="<?php echo esc_url( home_url() ); ?>">

			            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

			          </a>

			        <?php endif; ?>

			      <?php endif; ?>

			    </div>

			    <div class="mobile-logo">

			    	<?php if( function_exists( 'get_field' ) ): ?>

							<?php

			          $image = get_field( 'mobile_logo', 'options' );
			          $size = 'logo';
			          $src = $image['url'];
			          $alt = $image['alt'];
			          $title = $image['title'];
			          $thumb = $image['sizes'][ $size ];
			          $caption = $image['caption'];

			        if( $image ): ?>

			          <a href="<?php echo esc_url( home_url() ); ?>">

			            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">

			          </a>

			        <?php endif; ?>

			      <?php endif; ?>

			    </div>

		    </div>

		    <?php if( have_rows( 'social_media', 'options' ) ): ?>

		    	<div class="site-header__social">

		    		<?php while( have_rows( 'social_media', 'options' ) ): the_row(); ?>

		    			<a class="site-header__social-icon" href="<?php the_sub_field( 'social_link', 'options' ); ?>">
		    				<?php the_sub_field( 'social_icon', 'options' ); ?>
		    			</a>

		    		<?php endwhile; ?>

		    	</div>

		    <?php endif; ?>

				<nav role="navigation" class="desktop-menu">

		      <?php
					 	$defaults = array(
						 	'container' => false,
						 	'div' => false,
						 	'theme_location' => 'main-menu'
					 	);
					 wp_nav_menu( $defaults );
				 	?>

				</nav>

				<div class="mobile-menu clearfix">

					<button tabindex="0" onclick="openNav()" class="toggle-sidebar">
						<div class="hamburger-label">Menu</div>
						<div class="hamburger" style="max-width: 40px;">

							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
								<g>
									<g>
										<path class="st0" d="M491.3,235.3H20.7C9.3,235.3,0,244.6,0,256s9.3,20.7,20.7,20.7h470.6c11.4,0,20.7-9.3,20.7-20.7
											C512,244.6,502.7,235.3,491.3,235.3z"/>
									</g>
								</g>
								<g>
									<g>
										<path class="st0" d="M491.3,78.4H20.7C9.3,78.4,0,87.7,0,99.1s9.3,20.7,20.7,20.7h470.6c11.4,0,20.7-9.3,20.7-20.7
											S502.7,78.4,491.3,78.4z"/>
									</g>
								</g>
								<g>
									<g>
										<path class="st0" d="M491.3,392.2H20.7C9.3,392.2,0,401.5,0,412.9s9.3,20.7,20.7,20.7h470.6c11.4,0,20.7-9.3,20.7-20.7
											S502.7,392.2,491.3,392.2z"/>
									</g>
								</g>
							</svg>

						</div>
					</button>

					<div id="mySidenav" class="sidenav">
						<div class="sidenav-wrapper">

							<div id="sidebar">
								<div class="mobile-nav-header clearfix">
									<div class="close-sidenav clearfix">
										<button tabindex="0" class="closebtn" onclick="closeNav()">

											<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/close.svg" />

										</button>

									</div>
								</div>

								<div class="mobile-navigation">

									<nav class="mobile-menu" role="navigation">

										<?php
										 	$defaults = array(
											 	'container' => false,
											 	'div' => false,
											 	'theme_location' => 'hamburger-menu'
										 	);
										 wp_nav_menu( $defaults );
									 	?>

									</nav>

								</div>
							</div>
						</div>
					</div>

					<script>
						/* Set the width of the side navigation to 250px */
						function openNav() {
							jQuery('.sidenav').toggleClass('sidenav-open');
						}

						/* Set the width of the side navigation to 0 */
						function closeNav() {
							jQuery('.sidenav').toggleClass('sidenav-open');
						}
					</script>

				</div>

			</div>
		</div>

  </header>

  <?php if( is_front_page() ): ?>

  	<div class="video-header">

  		<div class="video-header__background">
  			<?php 
  				$mp4 = get_field( 'video_mp4' );
  				$webm = get_field( 'video_webm' );
  				$image = get_field( 'image_fallback' );
	        $size = 'full-page-width';
	        $src = $image['url'];
	        $alt = $image['alt'];
	        $thumb = $image['sizes'][ $size ];
	        $background = wp_get_attachment_image_src( get_field( 'background_image' ), 'full-page-width' );
  			?>

  			<video id="video" playsinline autoplay loop muted>
	  			<source src="<?php echo $mp4['url']; ?>" type="video/mp4">
	  			<source src="<?php echo $webm['url']; ?>" type="video/webm">

  				<?php if( $image ): ?>

		        <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" title="Your browser does not support the <video> tag">

		      <?php endif; ?>

	  		</video>

	  		<script>

	  			var video = document.getElementById('video');

					video.addEventListener('loadeddata', function() {
					    video.play();
					});

					video.addEventListener('pause', function() {
					    video.play();
					});

	  		</script>
  		</div>

  		<div class="video-header__image" style="background-image: url(<?php echo $background[0]; ?>);">
	  	</div>

  		<div class="video-header__content">

	  		<div class="video-header__tagline extra-large-text">

	  			<?php the_field( 'tagline' ); ?>

	  		</div>

	  		<div class="video-header__body">
	  			<?php the_field( 'video_header_text' ); ?>
	  		</div>

	  		<div class="button-wrap">

		  		<a class="button button--primary" href="<?php the_field( 'button_1_page_link' ); ?>"><?php the_field( 'button_1_text' ); ?></a>

		  		<a class="button button--secondary" href="<?php the_field( 'button_2_page_link' ); ?>"><?php the_field( 'button_2_text' ); ?></a>

		  	</div>

		  </div>
  		
  	</div>

  <?php else: ?>

  	<?php get_template_part( 'template-parts/content', 'page-banner' ); ?>

  <?php endif; ?>

  <div id="content" class="site-content">
