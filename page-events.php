<?php /* Template Name: Events */ ?>

<?php get_header(); ?>

  <main class="site-main" role="main">
    <div class="page-content-wrapper">
      <div class="padding-wrapper">

        <?php if( have_posts() ): ?>

          <?php while( have_posts() ): the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

              <div class="medium-wrapper">

                <div class="entry-content">

                  <?php the_content(); ?>

                </div>

              </div>

            </article>

          <?php endwhile; ?>

        <?php endif; ?>

      </div>
    </div>
  </main>

<?php get_footer(); ?>
