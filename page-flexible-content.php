<?php /* Template Name: Flexible Content */ ?>

<?php get_header(); ?>

  <main class="site-main" role="main">

    <?php if( function_exists( 'get_field' ) ): ?>

      <?php if( have_rows( 'content_blocks' ) ): ?>

        <?php while( have_rows( 'content_blocks' ) ): the_row(); ?>

          <?php if( get_row_layout() == 'icon_left' ): ?>

            <?php get_template_part('template-parts/content-block', 'icon-left'); ?>

          <?php elseif( get_row_layout() == 'image_background_with_buttons' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'image-background-with-buttons' ); ?>

          <?php elseif( get_row_layout() == 'image_left' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'image-left' ); ?>

          <?php elseif( get_row_layout() == 'basic_text_block' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'basic-text' ); ?>

          <?php elseif( get_row_layout() == 'centered_text_block' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'centered-text-block'); ?>

          <?php elseif( get_row_layout() == 'quote_section' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'quote-section'); ?>

          <?php elseif( get_row_layout() == 'icon_list' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'icon-list' ); ?>

          <?php elseif( get_row_layout() == 'page_list' ): ?>

            <?php get_template_part( 'template-parts/content-block', 'page-list' ); ?>

          <?php endif; ?>

        <?php endwhile; ?>

      <?php endif; ?>

    <?php endif; ?>

  </main>

<?php get_footer(); ?>
